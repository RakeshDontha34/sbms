
"""
Created on Thu Dec  16 03:05:56 2020

@author: Hari_Ramaraju.

@version : V1.0
"""

import pandas as pd
import logging
import time

from Src.DBConnect import DBConnect
from Src.DescriptiveAnalytics import generateDescriptiveAnalytics

try:
    Obj_DB = DBConnect()
    sourceDB_connection = Obj_DB.getSourceDBconnection()
    destinationDB_connection = Obj_DB.getDestinationDBconnection() 
    
except Exception as e:
    
        print(' production database insertion failed for ',sourceDB_connection)
        logging.exception(' production database insertion failed for ',destinationDB_connection)
        print(e)
        logging.error('Error at %s', 'division', exc_info=e)
    


def pushRecordTODescriptiveTable(descriptive_push_df,destinationDB_connection):
    try:
        descriptive_push_df.to_sql(con=destinationDB_connection, name='descriptive_table',if_exists='append',index=False)
    except Exception as e:
        print('production database insertion failed for ', get_id)
        print(e)
        logging.error('Error at %s', 'division', exc_info=e)
        
def UpdateRecordTODescriptiveTable(descriptive_push_df,destinationDB_connection):
    run_sql_stmt="update descriptive_table set unique_id ="+"'"+str(descriptive_push_df['unique_id'].iloc[0])+"'"+", live_count = "+"'"+str(descriptive_push_df['live_count'].iloc[0])+"'"+", asset_token= "+"'"+str(descriptive_push_df['asset_token'].iloc[0])+"'"+", date = "+"'"+str(descriptive_push_df['date'].iloc[0])+"'"+", no_of_cranks = "+"'"+str(descriptive_push_df['no_of_cranks'].iloc[0])+"'"+", min_voltage = "+"'"+str(descriptive_push_df['min_voltage'].iloc[0])+"'"+", max_voltage = "+"'"+str(descriptive_push_df['max_voltage'].iloc[0])+"'"+", min_current = "+"'"+str(descriptive_push_df['min_current'].iloc[0])+"'"+", max_current = "+"'"+str(descriptive_push_df['max_current'].iloc[0])+"'"+", min_temperature = "+'"'+str(descriptive_push_df['min_temperature'].iloc[0])+'"'+", max_temperature = "+'"'+str(descriptive_push_df['max_temperature'].iloc[0])+'"'+", soh = "+"'"+str(descriptive_push_df['soh'].iloc[0])+"'"+" where "+"("+" unique_id = "+"'"+str(descriptive_push_df['unique_id'].iloc[0])+"'"+" and date = "+"'"+str(descriptive_push_df['date'].iloc[0])+"');"
    destinationDB_connection.execute(run_sql_stmt)
    #print(run_sql_stmt)
    print("Record updated in descriptive table")

def getMinMaxLiveCount(df,date):
    sel_df = df[df['date']!=date]
    if(len(sel_df)>0):
        min_livecount = min(sel_df['live_count'])
        max_livecount = max(sel_df['live_count'])
    else:
        min_livecount = 0
        max_livecount = 0
    return min_livecount,max_livecount


while True:
    print('Batch Process Started...')
    device_UIDs =pd.read_sql('select distinct unique_id from a_module_status_dup',con=sourceDB_connection)

    device_UIDs_lst = device_UIDs['unique_id'].to_list()
    for get_id in device_UIDs_lst: 
        production_df = pd.read_sql('select unique_id,asset_token,live_count,cloud_time,voltage,current_nc,temperature,soh,descriptive_processed_flag from a_module_status_dup where unique_id = '+"'"+str(get_id)+"'"+" and descriptive_processed_flag = "+"'"+str(0)+"'",con=destinationDB_connection)
        #timestamp_df = pd.read_sql('select live_count,cloud_time from timestamp_table where unique_id = "{0}" and live_count <= {1}'.format(get_id,max(production_df1['live_count'])),con=destinationDB_connection)
        #production_df = pd.merge(production_df1,timestamp_df,on='live_count',how='inner')
        descriptive_df_fromTable = pd.read_sql('select unique_id,date,live_count from descriptive_table where unique_id = '+"'"+str(get_id)+"'"+" order by live_count desc limit 1 ",con=destinationDB_connection)
        #print(len(descriptive_df_fromTable))
        if(len(descriptive_df_fromTable)> 0):
            if(descriptive_df_fromTable.live_count.iloc[0] == production_df.live_count.iloc[-1]):
                print("No new record for asset id: ",production_df.asset_token.iloc[0])
                continue
            else:
                descriptive_df_fromTable["date"] = pd.to_datetime(descriptive_df_fromTable["date"])
                descriptive_lastUpdDate = str(descriptive_df_fromTable["date"][0].date())
        else:
            descriptive_lastUpdDate = None

        if (len(production_df)>0):
            production_df.rename(columns={'current_nc':'current'}, inplace=True)
            
            descriptive_df_ToTable,unique_dates_list = generateDescriptiveAnalytics(production_df)
            
            for i in range(len(descriptive_df_ToTable)):
                descriptive_push_df = pd.DataFrame(descriptive_df_ToTable.iloc[[i]])
                

                if(descriptive_lastUpdDate != descriptive_push_df['date'].iloc[0]):
                    pushRecordTODescriptiveTable(descriptive_push_df,destinationDB_connection)
                else:
                    UpdateRecordTODescriptiveTable(descriptive_push_df,destinationDB_connection)
            unique_id =production_df['unique_id'][0]
            
            production_df['cloud_time'] = pd.to_datetime(production_df['cloud_time'])
            production_df['date'] = production_df["cloud_time"].map(lambda t: t.date())
            
            min_livecount,max_livecount = getMinMaxLiveCount(production_df,unique_dates_list[-1])
            if max_livecount != 0:
                print('MinMax_LiveCount',min_livecount,':',max_livecount)
                run_sql_stmt = "update a_module_status_dup set descriptive_processed_flag ="+"'"+str(1)+"'"+" where "+'('+"unique_id = "+"'"+str(unique_id)+"'"+" and "+'('+"live_count between "+"'"+str(min_livecount)+"'"+" and "+"'"+str(max_livecount)+"'"+'));'
                destinationDB_connection.execute(run_sql_stmt)

    time.sleep(900)
                 