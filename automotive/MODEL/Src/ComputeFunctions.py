import logging
import json
import numpy as np
import pandas as pd

from Src.SBMS_Asset_Details import AssetDetails 
from Src.SBMS_SoC_Models import KFSoCEstimator
from Src.SBMS_SoH_Models import HistogramSoHEstimator
from Src.SBMS_RUL_Models import SigmoidRULPrediction

#Objects Functional Area
def ObjectsCreation(UID_list,Objs_dict,sba_refer_lookup):
    for id in UID_list:
        if id not in Objs_dict:
            Obj_SoC=KFSoCEstimator()
            Obj_SoH=HistogramSoHEstimator()
            Obj_RUL=SigmoidRULPrediction(sba_refer_lookup) 
            Obj_Asset_Info = AssetDetails()
            Objs_dict[id] = {"SOC":Obj_SoC,"SOH":Obj_SoH,'RUL':Obj_RUL,'Asset_Info':Obj_Asset_Info}
    return Objs_dict

def Reinit_Objects(Objs_dict,id,sba_refer_lookup):
    Obj_SoC=KFSoCEstimator()
    Obj_SoH=HistogramSoHEstimator()
    Obj_RUL=SigmoidRULPrediction(sba_refer_lookup) 
    Obj_Asset_Info = AssetDetails()
    Objs_dict[id] = {"SOC":Obj_SoC,"SOH":Obj_SoH,'RUL':Obj_RUL,'Asset_Info':Obj_Asset_Info}
    Objs_dict[id]["SOC"].init_soc_count=1
    return Objs_dict

def pushRecordTOProduction(production_push_df,destinationDB_connection,get_id,livecount):
    try:
        print('Insert Record for :', get_id)  
        production_push_df.to_sql(con=destinationDB_connection, name='a_module_status_dup_test_env',if_exists='append',index=False)
        print('Record Insrted Successfully... for:', get_id,' last live count ',livecount)
    except Exception as e:
        print('production database insertion failed for ', get_id)
        print(e)
        logging.error('Error at %s', 'division', exc_info=e)
        
def updateIntermid_lookup(new_uniqueid_flag,get_id,livecount,interMidLookup_data,destinationDB_connection):    
    if new_uniqueid_flag=='Y':
        run_sql_stmt = "insert into intermid_values_lookup_test_env (unique_id, live_count, cloud_time, soc_kf, soh, rul,min_rul,max_rul,rul_std, sigmax, crnk_soh, crank_info, past_data, past_data_index, initial_popt, initial_pcov) values ('{}',{},'{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}',{},'{}','{}')".format(interMidLookup_data['unique_id'][0],interMidLookup_data['live_count'][0],interMidLookup_data['cloud_time'][0],interMidLookup_data['soc_kf'][0],interMidLookup_data['soh'][0],interMidLookup_data['rul'][0],interMidLookup_data['min_rul'][0],interMidLookup_data['max_rul'][0],interMidLookup_data['rul_std'][0],interMidLookup_data['sigmax'][0],interMidLookup_data['crnk_soh'][0],interMidLookup_data['crank_info'][0],interMidLookup_data['past_data'][0],interMidLookup_data['past_data_index'][0],interMidLookup_data['initial_popt'][0],interMidLookup_data['initial_pcov'][0])       
    else:
        run_sql_stmt = "update intermid_values_lookup_test_env set unique_id='{}',live_count='{}',cloud_time='{}',soc_kf='{}', soh='{}', rul='{}',min_rul='{}',max_rul='{}',rul_std='{}', sigmax='{}', crnk_soh='{}', crank_info='{}', past_data='{}', past_data_index='{}', initial_popt='{}', initial_pcov='{}'".format(interMidLookup_data['unique_id'][0],interMidLookup_data['live_count'][0],interMidLookup_data['cloud_time'][0],interMidLookup_data['soc_kf'][0],interMidLookup_data['soh'][0],interMidLookup_data['rul'][0],interMidLookup_data['min_rul'][0],interMidLookup_data['max_rul'][0],interMidLookup_data['rul_std'][0],interMidLookup_data['sigmax'][0],interMidLookup_data['crnk_soh'][0],interMidLookup_data['crank_info'][0],interMidLookup_data['past_data'][0],interMidLookup_data['past_data_index'][0],interMidLookup_data['initial_popt'][0],interMidLookup_data['initial_pcov'][0]) +" where unique_id = "+"'"+str(interMidLookup_data['unique_id'][0])+"'"+";"                                        
    print(' intermid_values_lookup_test_env reached for ', get_id)  
    destinationDB_connection.execute(run_sql_stmt)
    print(' intermid_values_lookup_test_env last overwrite for ',get_id,' ',livecount) 
    
def getPrevious_Production_Values(df):
    prev_volt = df['voltage'].iloc[0]
    prev_curr = df['current_nc'].iloc[0]
    
    return prev_volt,prev_curr


def convertToarray(x):
    x = x.replace("["," ")
    x = x.replace("]"," ")
    x = x.split(" ")
    x = [float(i) for i in x if len(i)>1]
    x =  np.array(x,dtype=float).reshape(3,3)
    return x

def getPrevious_Intermid_lookup_Values(df):
    previous_soc_kf=df['soc_kf'].iloc[0]
    previous_sigmax = df['sigmax'].iloc[0]
    prev_soh=(float(df['soh'].iloc[0])/100)
    crnk_soh = df['crnk_soh'].iloc[0]
    crnk_soh=json.loads(crnk_soh)
    crank_info= df['crank_info'].iloc[0]
    #crank_info=crank_info.replace("'", '"')
    crank_info=json.loads(crank_info)
    rul_mean=int(df['rul'].iloc[0])
    rul_std=int(df['rul_std'].iloc[0])
    past_data = df['past_data'].iloc[0]
    if len(past_data)>0:
        #past_data=past_data.replace("'", '"')
        past_data=json.loads(past_data)
        past_data = pd.DataFrame.from_dict(past_data)
    past_data_index = int(df['past_data_index'].iloc[0])
    initial_popt = json.loads(df['initial_popt'].iloc[0])
    
    initial_pcov=convertToarray(df['initial_pcov'].iloc[0])
    return previous_soc_kf,previous_sigmax,prev_soh,crnk_soh,crank_info,rul_mean,rul_std,past_data,past_data_index,initial_popt,initial_pcov

def checkProd_RUL_MatchFailure(new_uniqueid_flag,get_id,capacity,Objs_dict,checkProd_RUL_df,intermid_lookup_df,sba_refer_lookup,destinationDB_connection):
    if len(checkProd_RUL_df)>0:
        if(len(intermid_lookup_df) > 0):
            Objs_dict[get_id]["SOH"].prev_volt,Objs_dict[get_id]["SOH"].prev_curr = getPrevious_Production_Values(checkProd_RUL_df)
            Objs_dict[get_id]["SOC"].previous_soc_kf,Objs_dict[get_id]["SOC"].previous_sigmax,Objs_dict[get_id]["SOH"].prev_soh,Objs_dict[get_id]["SOH"].crnk_soh,Objs_dict[get_id]["SOH"].crank_info,Objs_dict[get_id]["RUL"].rul_mean,Objs_dict[get_id]["RUL"].rul_std,Objs_dict[get_id]["RUL"].past_data,Objs_dict[get_id]["RUL"].past_data_index,Objs_dict[get_id]["RUL"].initial_popt,Objs_dict[get_id]["RUL"].initial_pcov = getPrevious_Intermid_lookup_Values(intermid_lookup_df)
            startFrom = 1
        else:
            Objs_dict = Reinit_Objects(Objs_dict,str(get_id),sba_refer_lookup)
            #print('Objs_dict ',Objs_dict)
            startFrom = 0
        prod_data_len=len(checkProd_RUL_df)
        print('Step2 in checkProd_RUL_MatchFailure')
        if prod_data_len>0:
            checkProd_RUL_df.drop_duplicates(subset ="live_count", keep ='first' , inplace = True)
            checkProd_RUL_df.sort_values(by=['live_count'], ascending=True,inplace = True)
            checkProd_RUL_df.reset_index(drop=True,inplace = True)
            for i in range(startFrom ,len(checkProd_RUL_df)):
                voltage=checkProd_RUL_df['voltage'][i]
                current=checkProd_RUL_df['current_nc'][i]
                livecount=checkProd_RUL_df['live_count'][i]
                uniqueid=checkProd_RUL_df['unique_id'][0]
                step_time=Objs_dict[get_id]["SOC"].get_step_time(current)
                cloud_time=checkProd_RUL_df['cloud_time'][0]

                
                Objs_dict[get_id]["SOC"].previous_soc_kf,Objs_dict[get_id]["SOC"].previous_sigmax = Objs_dict[get_id]["SOC"].kalman_filter(Objs_dict[get_id]["SOC"].previous_soc_kf,Objs_dict[get_id]["SOC"].previous_sigmax,voltage,current,step_time,capacity)
                Objs_dict[get_id]["SOH"].roll_soh,Objs_dict[get_id]["SOH"].crnk_soh,Objs_dict[get_id]["SOH"].crank_info = Objs_dict[get_id]["SOH"].get_soh(current,voltage,Objs_dict[get_id]["SOH"].prev_volt,Objs_dict[get_id]["SOH"].prev_curr,Objs_dict[get_id]["SOH"].prev_soh,Objs_dict[get_id]["SOH"].crnk_soh,Objs_dict[get_id]["SOH"].crank_info)
                soh=format(float(Objs_dict[get_id]["SOH"].roll_soh)*100,'.2f')
                Objs_dict[get_id]["SOH"].prev_soh,Objs_dict[get_id]["RUL"].rul_mean,Objs_dict[get_id]["RUL"].rul_std,Objs_dict[get_id]["RUL"].past_data,Objs_dict[get_id]["RUL"].past_data_index,Objs_dict[get_id]["RUL"].initial_popt,Objs_dict[get_id]["RUL"].initial_pcov = Objs_dict[get_id]["RUL"].get_rul(Objs_dict[get_id]["SOH"].prev_soh,Objs_dict[get_id]["SOH"].roll_soh,Objs_dict[get_id]["RUL"].past_data,Objs_dict[get_id]["RUL"].past_data_index,Objs_dict[get_id]["RUL"].rul_mean,Objs_dict[get_id]["RUL"].rul_std,Objs_dict[get_id]["RUL"].initial_popt,Objs_dict[get_id]["RUL"].initial_pcov)
                Objs_dict[get_id]["SOH"].prev_volt = voltage
                Objs_dict[get_id]["SOH"].prev_curr = current
                Objs_dict[get_id]["Asset_Info"].soc_status=Objs_dict[get_id]["Asset_Info"].getSoC_Status(Objs_dict[get_id]["SOC"].previous_soc_kf)
                Objs_dict[get_id]["Asset_Info"].soh_status=Objs_dict[get_id]["Asset_Info"].getSoH_Status(soh)
                
                print("get_id :",get_id,"SOC:", Objs_dict[get_id]["SOC"].previous_soc_kf,'SOH:',soh,'RUL:',Objs_dict[get_id]["RUL"].rul_mean,'\n')
                print('livecount  ',livecount)
                
                
                #checkProd_update_stmt=" update a_module_status_dup_test_env set soc_kf = "+str(format(float(Objs_dict[get_id]["SOC"].previous_soc_kf),'.2f'))+",soh = "+str(format(float(soh),'.2f'))+",rul = "+str(Objs_dict[get_id]["RUL"].rul_mean)+", rul_std = "+str(Objs_dict[get_id]["RUL"].rul_std)+" where unique_id = "+"'"+str(uniqueid)+"'"+" and live_count = "+str(livecount)+";"
                # checkProd_update_stmt=" update a_module_status_dup_test_env set soc_kf='{}',soh='{}',rul='{}',rul_std='{}'".format(format(float(Objs_dict[get_id]["SOC"].previous_soc_kf),'.2f'),format(float(soh),'.2f'),Objs_dict[get_id]["RUL"].rul_mean,Objs_dict[get_id]["RUL"].rul_std)+" where unique_id = "+"'"+str(uniqueid)+"'"+" and live_count = "+str(livecount)+";"
                # destinationDB_connection.execute(checkProd_update_stmt)
                if(i == (len(checkProd_RUL_df) -1)):
                    min_rul = Objs_dict[get_id]["RUL"].rul_mean - Objs_dict[get_id]["RUL"].rul_std
                    max_rul = Objs_dict[get_id]["RUL"].rul_mean + Objs_dict[get_id]["RUL"].rul_std      
                    interMidLookup_data=pd.DataFrame({'unique_id':uniqueid,'live_count':livecount,'cloud_time':str(cloud_time),'soc_kf':format(float( Objs_dict[get_id]["SOC"].previous_soc_kf),'.2f'),'soh':format(float(soh),'.2f'),'rul':Objs_dict[get_id]["RUL"].rul_mean,'min_rul': min_rul,'max_rul':max_rul,'rul_std':Objs_dict[get_id]["RUL"].rul_std,'sigmax':format(float(Objs_dict[get_id]["SOC"].previous_sigmax),'.4f'),'crnk_soh':str(Objs_dict[get_id]["SOH"].crnk_soh),'crank_info':json.dumps(Objs_dict[get_id]["SOH"].crank_info),'past_data':[Objs_dict[get_id]["RUL"].past_data.to_json()],'past_data_index':Objs_dict[get_id]["RUL"].past_data_index,'initial_popt':str(Objs_dict[get_id]["RUL"].initial_popt),'initial_pcov':str(Objs_dict[get_id]["RUL"].initial_pcov)},index=[0])
                    updateIntermid_lookup(new_uniqueid_flag,get_id,livecount,interMidLookup_data,destinationDB_connection)

                
                print('Step3 Updating RUL_Lookup in checkProd_RUL_MatchFailure')
                
                
        print('Step4 in checkProd_RUL_MatchFailure')

def multiply_current(u_id,current,NoCorrectionFact_UDIs_lst):
    if u_id in NoCorrectionFact_UDIs_lst:
        new_current=current
    else:
        new_current=current*1.3334
    return format(float(new_current),'.4f')

