import sqlalchemy


class DBConnect:
    
    def __init__(self):
        print('DB connection Object created...')

    def getSourceDBconnection(self):
        src_db_uname = 'zaki'
        src_db_pwd = 'Zaki@123'
        src_db_ip      = '20.75.89.19'
        src_db_name     = 'sbms_stg'
        port = 3306
        db_source_conn = sqlalchemy.create_engine('mysql+mysqlconnector://{0}:{1}@{2}:{3}/{4}'.format(src_db_uname, src_db_pwd,src_db_ip,port,src_db_name))
        return db_source_conn
    
    def getDestinationDBconnection(self):
        des_db_uname = 'zaki'
        des_db_pwd = 'Zaki@123'
        des_db_ip    = '20.75.89.19'
        des_db_name    = 'sbms_stg'
        port = 3306
        db_desti_conn = sqlalchemy.create_engine('mysql+mysqlconnector://{0}:{1}@{2}:{3}/{4}'.format(des_db_uname, des_db_pwd,des_db_ip,port,des_db_name))
        return db_desti_conn
    
    
    def closeDBconnection(self):
        print('Connection Closed...')
        
    
        

