import pandas as pd
from datetime import datetime,date
from datetime import timedelta
#12-01-2022
class Timestamping:
    def __init__(self):
        self.lastTimestamp = None
        self.lastLiveCount = 0
        
    def add_mode_step(self,data):
        data['temp_mobile_time'] = pd.to_datetime(data['temp_mobile_time'])
        data['mode'] = 'None'
        data['step_time'] = 0
        for i in range(len(data)):
            if data['current'][i]<-100:
                data.loc[i,'mode'] = 'Crank'
                data.loc[i,'step_time'] = 0.01
            elif (data['current'][i]>=-100) & (data['current'][i]<=-25):
                data.loc[i,'mode'] = 'Discharge'
                data.loc[i,'step_time'] = 0.05
            elif (data['current'][i]>-25) & (data['current'][i]<-1):
                data.loc[i,'mode'] = 'Discharge'
                data.loc[i,'step_time'] = 60
            elif data['current'][i] > 0:
                data.loc[i,'mode'] = 'Charge'
                data.loc[i,'step_time'] = 60
            else:
                data.loc[i,'mode'] = 'Idle'
                data.loc[i,'step_time'] = 60
        return data
    
    def create_dictionary(self,keys,values):
        result = {} # empty dictionary
        for key, value in zip(keys, values):
            result[key] = value
        return result

    def get_timestamps_available(self,df):
        timestamp_df = df.loc[pd.isnull(df.temp_mobile_time) == False,['live_count','temp_mobile_time']]
        timestamp_dictionary = self.create_dictionary(timestamp_df.live_count.tolist(),timestamp_df.temp_mobile_time.tolist())
        return timestamp_dictionary

    def get_min_max_livecount_data(self,df):
        min_livecount_df, max_livecount_df = min(df.live_count),max(df.live_count)
        return min_livecount_df, max_livecount_df

    def get_idle_period_df(self,df):
        arr = df.loc[df['mode']=='Idle','live_count'].to_list()
        n = len(arr)
        s = set()
        longest_idle_period = 0

        # Hash all the array elements
        for ele in arr:
            s.add(ele)
        #print('s':s)
        min_list = []
        max_list = []
        length_list = []
        mode_change_list = []
        for i in range(n):
            if (arr[i]-1) not in s:
                j = arr[i]
                req_list = []
                while(j in s):
                    req_list.append(j)
                    j += 1
                if len(req_list)>=60:
                    min_list.append(min(req_list))
                    max_list.append(max(req_list))
                    length_list.append(len(req_list))
                elif len(req_list)<5:                  ## code addition
                    mode_change_list.append(req_list)
                longest_idle_period = max(longest_idle_period, j-arr[i])
                
        idle_period_df = pd.DataFrame({'min_livecount':min_list,'max_livecount':max_list,'idle_length':length_list})
        return idle_period_df,mode_change_list

    def get_rest_period_info(self,df,timestamp_value_list,timestamp_livecount_list,rest_livecount_list):
        time_difference = (timestamp_value_list[1] - timestamp_value_list[0]).total_seconds()
        total_active_period = sum(df.loc[(df['live_count']>timestamp_livecount_list[0]) & (df['live_count']<timestamp_livecount_list[1]),'step_time'])
        found = list(filter(lambda x: timestamp_livecount_list[0] <= x <= timestamp_livecount_list[1], rest_livecount_list))
        k = len(found)
        rest_period = time_difference - total_active_period
        return rest_period,k
    
    def rest_flag_change(self,rest_livecount_list,variable):
        if (variable in rest_livecount_list):
            return 1
        else:
            return 0
        
    def get_mode_correction(self,df,mode_change_list):  ## code addition
        for set_id in range(len(mode_change_list)):
            if len(mode_change_list[set_id]) >=5:
                if  all(df.loc[(df['live_count']>=mode_change_list[set_id][0]) & (df['live_count']<=mode_change_list[set_id][-1]),'voltage']>13) == True:
                    for sub_set_id in mode_change_list[set_id]:
                        df.loc[df['live_count']==sub_set_id,'mode'] = 'Discharge'
                        df.loc[df['live_count']==sub_set_id,'step_time'] = 60 #0.05
                else:
                    continue
            else:
                for sub_set_id in mode_change_list[set_id]:
                    #if df.loc[df['live_count']==sub_set_id,'voltage'].item() > 13:
                    df.loc[df['live_count']==sub_set_id,'mode'] = 'Discharge'
                    df.loc[df['live_count']==sub_set_id,'step_time'] = 60 #0.05
        return df

    def get_initial_timestamps(self,df,main_timestamp_dict,min_lc,timestamp_livecount,timestamp_value):
        req_df = df[(df['live_count']>=min_lc) & (df['live_count']<timestamp_livecount)]
        req_df.reset_index(drop=True,inplace=True)
        key_list = []
        value_list = []
        last_timestamp = timestamp_value
        for i in reversed(range(len(req_df))):
            new_timestamp = last_timestamp - timedelta(seconds = float(req_df['step_time'][i]))
            value_list.append(new_timestamp)
            key_list.append(req_df['live_count'][i])
            last_timestamp = new_timestamp
        value_list.reverse()
        key_list.reverse()
        main_timestamp_dict.update(self.create_dictionary(key_list,value_list))
        return main_timestamp_dict

    def get_timestamp_norest(self,df,main_timestamp_dict,timestamp_livecount_list,timestamp_value_list):
        req_df = df[(df['live_count']>timestamp_livecount_list[0]) & (df['live_count']<=timestamp_livecount_list[1])]
        req_df.reset_index(drop=True,inplace=True)
        last_timestamp = timestamp_value_list[0]
        key_list = [timestamp_livecount_list[0]]
        value_list = [last_timestamp]
        if len(req_df)<2:
            main_timestamp_dict.update(self.create_dictionary(key_list,value_list))
        else:
            for i in range(len(req_df)-1):
                #print(req_df['step_time'][i])
                new_timestamp = last_timestamp + timedelta(seconds = float(req_df['step_time'][i]))
                value_list.append(new_timestamp)
                key_list.append(req_df['live_count'][i])
                last_timestamp = new_timestamp
            main_timestamp_dict.update(self.create_dictionary(key_list,value_list))
        return main_timestamp_dict


    def get_timestamp_with_k_rest(self,df,main_timestamp_dict,timestamp_value_list,timestamp_livecount_list,rest_livecount_list,k,rest_period):
        avg_rest_period = rest_period/k
        rest_flag = 0
        found = list(filter(lambda x: timestamp_livecount_list[0] <= x <= timestamp_livecount_list[1], rest_livecount_list))
        timestamp_change_livecount_list = sorted(list(set(timestamp_livecount_list + found)))
        ##   T[1,100].... R[20,50]
        
        while len(timestamp_change_livecount_list) > 1:
            last_timestamp = timestamp_value_list[0]
            rest_flag = self.rest_flag_change(rest_livecount_list,timestamp_change_livecount_list[0])
            if rest_flag == 0:
                req_df = df[(df['live_count']>timestamp_change_livecount_list[0]) & (df['live_count']<=timestamp_change_livecount_list[1])]
                req_df.reset_index(drop=True,inplace=True)
                key_list = [timestamp_change_livecount_list[0]]
                value_list = [last_timestamp]
                for i in range(len(req_df)-1):
                    new_timestamp = last_timestamp + timedelta(seconds = float(req_df['step_time'][i]))
                    value_list.append(new_timestamp)
                    key_list.append(req_df['live_count'][i])
                    last_timestamp = new_timestamp
                main_timestamp_dict.update(self.create_dictionary(key_list,value_list))
                #print(req_df['step_time'][-1:])
                last_timestamp = last_timestamp + timedelta(seconds = req_df['step_time'][-1:].item())
                timestamp_value_list.pop(0)
                timestamp_value_list.insert(0,last_timestamp)
                timestamp_change_livecount_list.pop(0)
            else: 
                #print(timestamp_change_livecount_list[:2])
                req_df = df[(df['live_count']>timestamp_change_livecount_list[0]) & (df['live_count']<=timestamp_change_livecount_list[1])]
                req_df.reset_index(drop=True,inplace=True)
                key_list = [timestamp_change_livecount_list[0]]
                value_list = [last_timestamp]
                last_timestamp = last_timestamp + timedelta(seconds=avg_rest_period)
                for i in range(len(req_df)-1):
                    new_timestamp = last_timestamp + timedelta(seconds = float(req_df['step_time'][i]))
                    value_list.append(new_timestamp)
                    key_list.append(req_df['live_count'][i])
                    last_timestamp = new_timestamp
                main_timestamp_dict.update(self.create_dictionary(key_list,value_list))
                
                #print('i:',i,'last_step_time:',req_df['step_time'][-1:].item())
                last_timestamp = last_timestamp + timedelta(seconds = req_df['step_time'][-1:].item())
                    
                timestamp_value_list.pop(0)
                timestamp_value_list.insert(0,last_timestamp)
                timestamp_change_livecount_list.pop(0)
        return main_timestamp_dict
    
    def get_timestamp_df(self,df):
        main_timestamp_dict ={}
        df = self.add_mode_step(df)
        print('No. of records from of Staging df after mode addition: ',df.shape[0])
        min_livecount_df, max_livecount_df = self.get_min_max_livecount_data(df)
        print('minimum livecount: ',min_livecount_df)
        print('maximum livecount: ',max_livecount_df)
        timestamp_dictionary = self.get_timestamps_available(df)
        timestamp_livecount_list = list(timestamp_dictionary.keys())
        timestamp_value_list = list(timestamp_dictionary.values())
        idle_period_df,mode_change_list = self.get_idle_period_df(df)
        df = self.get_mode_correction(df,mode_change_list)
        rest_livecount_list = (idle_period_df['max_livecount']).to_list()

        if (timestamp_livecount_list[0] > min_livecount_df):
            print('Inside initial timestamping Code')
            main_timestamp_dict = self.get_initial_timestamps(df,main_timestamp_dict,min_livecount_df,timestamp_livecount_list[0],timestamp_value_list[0])

        while (len(timestamp_livecount_list) > 1):

            rest_period,k = self.get_rest_period_info(df,timestamp_value_list,timestamp_livecount_list,rest_livecount_list)
            
            if k == 0 :
                print('Inside zero rest period timestamping Code')
                main_timestamp_dict= self.get_timestamp_norest(df,main_timestamp_dict,timestamp_livecount_list[:2],timestamp_value_list[:2])
            else:
                print('Inside k rest period timestamping Code')
                main_timestamp_dict= self.get_timestamp_with_k_rest(df,main_timestamp_dict,timestamp_value_list[:2],timestamp_livecount_list[:2],rest_livecount_list,k,rest_period)
            timestamp_value_list.pop(0)
            timestamp_livecount_list.pop(0) 
        
        main_timestamp_df = pd.DataFrame(main_timestamp_dict.items(),columns = ['live_count','timestamp'])
        return main_timestamp_df