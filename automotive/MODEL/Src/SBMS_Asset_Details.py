# -*- coding: utf-8 -*-
"""
Created on Thu Dec  16 03:05:56 2020

@author: Ram Prasad,Kalyan,Hari Ramaraju.
"""

class AssetDetails:
    def __init__(self):
        self.soc_status = 'good'
        self.soh_status = 'good'
        print('Asset Details Object Created...')
    def getSoH_Status(self,soh):
        if soh==None or soh =='' or soh==' ':
            send_status="inactive"
        if float(soh) > 90:
            send_status="good"
        elif float(soh) > 70 and float(soh) <= 90:
            send_status="stable"
        else:
            send_status="critical"
        return send_status  
    def getSoC_Status(self,soc):
        if float(soc) > 80:
            status='good'
        elif float(soc) > 60 and float(soc) <= 80:
            status ='stable'
        else:
            status='critical'
        return status
    





