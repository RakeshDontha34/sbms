
"""
Created on Wed Oct  21 23:36:54 2020

@author: Hari_Ramaraju
@version : V1.0
"""


import matplotlib.pyplot as plt 
import os

#Functonal Area
def generatePlots(data,plotsPath,DAS_list):
    for dlst in DAS_list:
        if not os.path.exists(plotsPath+""+dlst):
            os.makedirs(plotsPath+""+dlst)
        for col in ['soc_kf','soh','RUL']:
            plt.figure(figsize=(20,6))
            if col != 'RUL':
                plt.ylim([0,105])
            plt.plot(data[col])
            plt.title(col)
            plt.xlabel('TimeStamp')
            plt.ylabel(col)
            print(plotsPath+""+dlst+'\\'+col+".png")
            plt.savefig(plotsPath+""+dlst+'\\'+col+".png")
            
