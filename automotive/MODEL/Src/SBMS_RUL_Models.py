# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
#import plotly.express as px
from scipy.optimize import curve_fit
from scipy.special import expit

global referable_parameters_lookup_dict 
global socdata_lookup_dict
global oem_specifications_lookup_dict
global max_rul
global const_rul_std

class SigmoidRULPrediction:
    def __init__(self,reference_data):
        self.max_rul =16500
        self.const_rul_std = 1000
        self.rul_mean = 0
        self.rul_std = 0
        self.reference_data = reference_data
        self.past_data = pd.DataFrame()
        self.past_data_index = 0
        self.initial_popt = [ 0.03988567,  0.8267236 , -1.64732671] 
        self.initial_pcov = np.array([[ 3.68085371e-06, -1.68289863e-05, -2.42405078e-05],
                         [-1.68289863e-05,  7.72724170e-05,  1.08481414e-04],
                         [-2.42405078e-05,  1.08481414e-04,  1.80583482e-04]])
        print('RUL Object created.')
    
    def get_past_data(self,reference_data,latest_soh):
        reference_data = reference_data[['Cycle_Index','SOH']]
        past_data = (reference_data.loc[reference_data['SOH']> latest_soh]).copy()
        past_data_index = len(past_data)
        return past_data,past_data_index
    
    def inverse_sigmoid_func(self,x,a,c,b):
        exp_var = a*(x**c) + b
        #print(exp_var)
        return expit(-exp_var)
    
    def curveFitEstimate(self,data,initial_popt,initial_pcov):
        X = ((data["Cycle_Index"].iloc[:])/100).values
        y = data["SOH"].iloc[:].values
        try:
            popt, pcov = curve_fit(self.inverse_sigmoid_func, X, y, p0=initial_popt,maxfev = 5000)
        except RuntimeError:
            #print("No fit found")
            popt = initial_popt
            pcov = initial_pcov
        #popt, pcov = curve_fit(inverse_sigmoid_func, X, y, p0=initial_popt,maxfev = 10000)
        #print("estimated popt",popt)
        if((popt[0]>1) or (popt[0] <0 )  or (popt[1] < 1.2) ):
            popt = initial_popt
            pcov = initial_pcov
            #print(popt)
        return popt, pcov
    
    def getRUL_curveFit_expo(self,popt,pcov,threshold=0.3):
        try:
            pcov=pcov.replace("'", '')
        except:
            pcov=pcov
        print(' line 52 ', pcov,' type ', type(pcov))
        std_var = np.sqrt(np.diag(np.abs(pcov)))
        print(' line 54 ', pcov,' type ', type(pcov))
        mean_var = popt
        a = np.random.normal(mean_var[0], std_var[0], 4000)
        c = np.random.normal(mean_var[1], std_var[1], 4000)
        b = np.random.normal(mean_var[2], std_var[2], 4000)
        rul_mean =(((np.log((1/threshold)-1) - b)/a)**(1/c)).mean()
        rul_std = (((np.log((1/threshold)-1) - b)/a)**(1/c)).std()
        return rul_mean,rul_std
    
    def get_rul(self,prev_soh,latest_soh,past_data,past_data_index,latest_rul,latest_rul_std,initial_popt,initial_pcov):
        if (latest_soh == 1):
            #print('inside if loop')
            latest_rul = self.max_rul
            latest_rul_std = self.const_rul_std
        elif((latest_soh > 0.7) and (latest_soh <1)):
            if ((latest_soh < prev_soh) or (abs(prev_soh-latest_soh) > 0.01)):
                #print('inside elifif loop')
                past_data, past_data_index = self.get_past_data(self.reference_data,latest_soh)
                latest_rul = self.max_rul - past_data_index
                latest_rul_std = self.const_rul_std
                #prev_soh = latest_soh
                #print(past_data_index,latest_soh)
            else:
                #print('inside elifelse loop')
                latest_rul = self.max_rul - past_data_index
                latest_rul_std = self.const_rul_std
                #print(prev_soh,latest_soh)
                
        elif(latest_soh <= 0.7):
            
            if ((past_data_index == 0) or (len(past_data) == 0) or (abs(prev_soh-latest_soh) > 0.02)):
                past_data, past_data_index = self.get_past_data(self.reference_data,latest_soh)
                latest_rul = self.max_rul - past_data_index
                latest_rul_std = self.const_rul_std
            
            elif (latest_soh < prev_soh):
                
                #print("inside elifif2 loop")
                past_data_index +=  1
                #print('soh:',latest_soh,'cycleind:',past_data_index)
                past_data = past_data.append(pd.DataFrame({'Cycle_Index':[past_data_index],'SOH':[latest_soh]}),ignore_index = True)
                #print(past_data)
                popt, pcov = self.curveFitEstimate(past_data,initial_popt,initial_pcov)
                print(' line 96 ', pcov,' type ', type(pcov))
                initial_popt = popt
                initial_pcov = pcov
                latest_rul,latest_rul_std = self.getRUL_curveFit_expo(popt,pcov)
                print(' line 99 ', pcov,' type ', type(pcov))
                #print('rul:',latest_rul)
                #print('rul:',latest_rul,"past_data_index:",past_data_index,"past_data_index",past_data_index)
                latest_rul = int((latest_rul*100) - past_data_index)  
    
                if(latest_rul>2000):
                    latest_rul_std = int(latest_rul_std*100*2)
                else:
                    latest_rul_std = int(latest_rul_std*100)
                #print('loop1: ',latest_rul)   
                #input("press enter to continue")
            #prev_soh = latest_soh
    
        return latest_soh,latest_rul,latest_rul_std,past_data,past_data_index,initial_popt,initial_pcov