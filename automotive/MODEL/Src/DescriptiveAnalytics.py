# -*- coding: utf-8 -*-
import pandas as pd
#import numpy as np
import warnings
warnings.filterwarnings('ignore')


## for consecutive cranks
def get_crank_data(data):
    crank_data = []
    volt = []
    curr = []
    for i in range(len(data)):
        if(data.current.iloc[i] > -95):
            if(len(volt) > 5):
                    crank_data.append(dict(voltage = volt,current = curr)) 
                    
            volt = [data.voltage.iloc[i]]
            curr = [data.current.iloc[i]]

        else:
            volt.append(data.voltage.iloc[i])
            curr.append(data.current.iloc[i])
            
    return crank_data


### Main Report Call
def max_min_of_parameters(data,parameters):
    list_min_max = []
    for col in parameters:
        data_min = round(data[col].min(),2)
        data_max = round(data[col].max(),2)
        list_min_max.append(data_min)
        list_min_max.append(data_max)
    return list_min_max
    

def report_features(clean_data,parameters): 
    unique_id = clean_data['unique_id'].iloc[0]
    live_count = clean_data['live_count'].iloc[-1]
    asset_token = clean_data['asset_token'].iloc[0]
    Date=clean_data.date.unique()[0]
    #print("date",Date)
    crank_counter =len(get_crank_data(clean_data))
    soh = clean_data['soh'].iloc[-1]
    min_max_list = max_min_of_parameters(clean_data,parameters)
    features_list = [str(unique_id),live_count,asset_token,Date,crank_counter,soh]
    features_list.extend(min_max_list)
    return features_list


def generateDescriptiveAnalytics(data):
    #data.rename(columns={'current_nc':'current'}, inplace=True)
    data=data[['unique_id','live_count','asset_token','voltage','current','temperature','cloud_time','soh']]
    #print(data)
    data['cloud_time'] = pd.to_datetime(data['cloud_time'])
    data['date'] = [d.date() for d in data['cloud_time']]
    ## Parameters used to find minimum and maximum ranges
    parameters=['voltage','current','temperature']
    ## Name of the columns in daywise report dataframe
    column=['unique_id','live_count','asset_token','date','no_of_cranks','soh','min_voltage','max_voltage','min_current','max_current','min_temperature','max_temperature']
    main_df = pd.DataFrame(columns =column)
    unique_dates_list = data.date.unique()
    for date_info in unique_dates_list:
        clean_df = data[data.date == date_info] 
        #print(clean_df)
        features_list = report_features(clean_df,parameters)
       
        df2 = pd.DataFrame({'unique_id':str(features_list[0]),'live_count':int(features_list[1]),'asset_token':str(features_list[2]),'date':str(features_list[3]),'no_of_cranks':features_list[4],'soh':features_list[5],'min_voltage':features_list[6],'max_voltage':features_list[7],'min_current':features_list[8],'max_current':features_list[9],'min_temperature':features_list[10],'max_temperature':features_list[11]},index=[0])
        df2
        main_df = main_df.append(df2)
        main_df.reset_index(inplace = True,drop= True)
    return main_df,unique_dates_list