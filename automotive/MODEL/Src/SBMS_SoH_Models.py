# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np

class SimilaritySoHEstimator:
    def __init__(self):
        self.referable_parameters_lookup_dict = {'name':['sba_test_regression'],
                                          'intercept':[1.49098883],
                                          'slope_cr':[-78.59719723]} 
        #self.crank_info = crank_info
        self.prev_soh = 1
        self.crnk_soh = []
        self.crank_info= {"Crank_curr": [],"Crank_volt": [],"prev_current": [],"prev_volt": []}
        self. prev_volt = 12.6
        #self.init_ocv_volt = 12.6
        self.prev_curr = 0
        print('SoH Object Created.')
        
    def get_Rolling_Window(self,length):
        if length <= 10:
            n = length
        elif length > 10:
            n = 10
        return n 
    def get_Roll_SoH_Mean(self,soh_list):
        if(len(soh_list) == 0):
            return 1
        else:
            required_soh_df = pd.DataFrame(soh_list)
            rolling_window_size = self.get_Rolling_Window(len(required_soh_df))
            soh_new = float((required_soh_df.rolling(rolling_window_size).mean())[-1:][0])
            return soh_new
    def clear_Crank_Data(self,crank_info):
        crank_info["Crank_curr"] = []
        crank_info["Crank_volt"] = []
        crank_info["prev_current"] = []
        crank_info["prev_volt"] = []
        return crank_info
    def append_Crank_Data(self,crank_info,current,voltage,prev_curr,prev_voltage):
        crank_info["Crank_curr"].append(current)
        crank_info["Crank_volt"].append(voltage)
        crank_info["prev_current"].append(prev_curr)
        crank_info["prev_volt"].append(prev_voltage)
        return crank_info
    def estimate_SoH(self,cr,slope_0,intercept):
        soh = slope_0*cr + intercept
        return soh
    def get_ir_prod(self,crank_data):
        crank_curr = min(crank_data["Crank_curr"])
        crank_volt = min(crank_data["Crank_volt"])
        prev_volt = float(12.6)                                             
        prev_current = crank_data["prev_current"][0]
        vdiff = prev_volt-crank_volt
        ir = vdiff/(prev_current-crank_curr)
        return ir
    def cal_soh(self,crank_info):
        #min_cr = referable_parameters_lookup_dict["min_cr"][0]
        #max_cr  = referable_parameters_lookup_dict["max_cr"][0]
        slope_0 = self.referable_parameters_lookup_dict["slope_cr"][0]
        intercept =self. referable_parameters_lookup_dict["intercept"][0]
        cr = self.get_ir_prod(crank_info)
        soh = self.estimate_SoH(cr,slope_0,intercept)
        return soh
    def checkSoHRange(self,df):
        if (df >1):
            df =1
        elif(df < 0):
            df =0
        return df
    def get_soh(self,current,Voltage,prev_volt,prev_curr,roll_soh,crnk_soh,crank_info):
        if(current > -95):
            if((len(crank_info["Crank_curr"]) > 5) and (crank_info["prev_volt"][0] < 12.8)):
                #print(crank_info)
                new_soh = self.cal_soh(crank_info)
                new_soh = self.checkSoHRange(new_soh)
                crnk_soh.append(new_soh)
                if(len(crnk_soh)> 10):
                    crnk_soh = crnk_soh[-10:]
                roll_soh = self.get_Roll_SoH_Mean(crnk_soh)
                #print(roll_soh)
            
            crank_info = self.clear_Crank_Data(crank_info)
            return roll_soh,crnk_soh,crank_info      
        else:
            crank_info = self.append_Crank_Data(crank_info,current,Voltage,prev_curr,prev_volt)
            return roll_soh,crnk_soh,crank_info    
        
        
class HistogramSoHEstimator:
    def __init__(self):
        self.stored_bins = [0.018955961,0.018712984,0.018458033000000002,0.018200456,0.017949886,0.01769552,0.017437358,
               0.017187856,0.016932422,0.016676795,0.016340167,0.016087287,0.015873197,0.015658943999999998,
               0.015406226,0.01515186,0.014516129,0.01388003,0.013243449,0.0126082,0.011971136,0.011335863,
               0.010698823,0.010060767,0.00942651,0.008788914,0.008154521999999999,0.00751818,
               0.006881653000000001]
        #self.crank_info = crank_info
        self.prev_soh = 1
        self.crnk_soh = []
        self.crank_info= {"Crank_curr": [],"Crank_volt": [],"prev_current": [],"prev_volt": []}
        self. prev_volt = 12.6
        #self.init_ocv_volt = 12.6
        self.prev_curr = 0
        print('SoH Object Created.')
        
    def get_Rolling_Window(self,length):                     ############## change
        num = 20
        if length <= num:
            n = length
    
        elif length > num:
            n = num
        return n 
    def get_roll_soh_mean(self,soh_list,last_roll_soh):                    ############## change
        if(len(soh_list) == 0):
            soh_new =  1
        else:
            soh_new = round(float(np.mean(soh_list)),3)
            if last_roll_soh < soh_new:
                soh_new = last_roll_soh
        return soh_new
    def clear_Crank_Data(self,crank_info):
        crank_info["Crank_curr"] = []
        crank_info["Crank_volt"] = []
        crank_info["prev_current"] = []
        crank_info["prev_volt"] = []
        return crank_info
    def append_Crank_Data(self,crank_info,current,voltage,prev_curr,prev_voltage):
        crank_info["Crank_curr"].append(current)
        crank_info["Crank_volt"].append(voltage)
        crank_info["prev_current"].append(prev_curr)
        crank_info["prev_volt"].append(prev_voltage)
        return crank_info
    
    def bin_soh(self,cr):                                    ############## change
        bin_num = np.digitize(cr,self.stored_bins)
        if bin_num<=15:
            soh = bin_num*(0.02)
        else:
            soh = (bin_num-15+6)*(0.05)
        return soh
    
    def get_ir_prod(self,crank_info):
        crank_curr = min(crank_info["Crank_curr"])
        crank_volt = min(crank_info["Crank_volt"])
        prev_volt = crank_info["prev_volt"][0] #float(12.6)                               ###### change again
        prev_current = crank_info["prev_current"][0]                        # mistake corrected
        vdiff = prev_volt-crank_volt
        ir = vdiff/(prev_current-crank_curr)
        return ir
    def cal_soh(self,crank_info):
        cr = self.get_ir_prod(crank_info)
        soh = round(self.bin_soh(cr),3)
        return soh
    def check_soh(self,soh):                                                # change
        if soh>=1:
            soh = 1
        elif soh<=0.3:                                   ############## change
            soh = 0.3
        return soh
    def get_soh(self,current,Voltage,prev_volt,prev_curr,roll_soh,crnk_soh,crank_info):
        if(current > -95 ):                                                                      # change
            if((len(crank_info["Crank_curr"]) > 5) and (crank_info["prev_volt"][0] < 12.8)):
                new_soh = self.cal_soh(crank_info)
                new_soh = self.check_soh(new_soh)                                                    # change
                crnk_soh.append(new_soh)
                if(len(crnk_soh)> 20):                                        ############## change
                    crnk_soh = crnk_soh[-20:]
                roll_soh = self.get_roll_soh_mean(crnk_soh,roll_soh)
            
            crank_info = self.clear_Crank_Data(crank_info)
        else:
            crank_info = self.append_Crank_Data(crank_info,current,Voltage,prev_curr,prev_volt)
        
        return roll_soh,crnk_soh,crank_info                       ############## change