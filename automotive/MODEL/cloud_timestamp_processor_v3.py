## importing required libraries
import pandas as pd
from datetime import datetime
import time
import logging
import warnings
warnings.filterwarnings('ignore')

#import local libraries.
from Src.DBConnect import DBConnect
from Src.calcfunction import Timestamping

#02-11-21: added CDAQS-21 TO THE LIST
#module_rawdata_test_env
#v_module_rawdata
#module_rawdata
#kurt_sample_data_2

srcDB_conn=None
destDB_conn=None

try:
    Obj_DB = DBConnect()
    srcDB_conn = Obj_DB.getSourceDBconnection()
    destDB_conn = Obj_DB.getDestinationDBconnection() 
    
except Exception as e:
        print(' production database insertion failed for ',srcDB_conn)
        logging.exception(' production database insertion failed for ',srcDB_conn)
        logging.error('Error at %s', 'division', exc_info=e)

'''
def UpdateStagingTable(timestamping_df,u_id,srcDB_conn):
    timestamping_df.reset_index(inplace = True,drop = True)
    for i in range(len(timestamping_df)):
        timestamp = timestamping_df['timestamp'].iloc[i]
        live_count = timestamping_df['live_count'].iloc[i]
        #print(live_count)
        run_sql_stmt='update sbms_stg.module_rawdata_test_env set cloud_time = CAST("{0}" AS DATETIME), cloudtime_modified = "{1}" where unique_id="{2}" and live_count={3}'.format(timestamp,'Y',u_id,live_count)
        srcDB_conn.execute(run_sql_stmt)
    final_livecount = live_count
    return final_livecount
'''
def pushTimestamp(timestamping_df,u_id,SourceDB_Connection):
    timestamping_df.reset_index(inplace = True,drop = True)
    timestamping_df['unique_id'] = u_id
    timestamping_df = timestamping_df.rename(columns={"timestamp": "cloud_time"})
    timestamping_df.to_sql(con=SourceDB_Connection, name='timestamp_table_test_env',if_exists='append',index=False)
    final_livecount = timestamping_df['live_count'].iloc[-1]
    return final_livecount

def createObjects(UID_list,Objs_dict):
    for id in UID_list:
        if id not in Objs_dict:
            Obj_Description = Timestamping()
            Objs_dict[id] = {"Obj_Description":Obj_Description}
    return Objs_dict

#Execution Start Point 

Objs_dict= {} 

while True:
    
    starttime=datetime.now()
    print('Process Started @...',starttime)
    
    unique_ids=pd.read_sql('select distinct unique_id from sbms_stg.module_rawdata_test_env where asset_token in ("{0}","{1}","{2}","{3}","{4}")'.format('SDAS-50','SDAS-51','CDAS-1','CDAS-23','CDAS-25'),con=srcDB_conn)
    print('time taken to get the distinct unique_ids from stg table: ',(datetime.now()-starttime).total_seconds())

    unique_ids_list=unique_ids['unique_id'].to_list()
    Objs_dict = createObjects(unique_ids_list,Objs_dict)

    for u_id in unique_ids_list:
        print('Unique ID under process:',u_id )
        udt1 = datetime.now()
        max_livecount_time=datetime.now()
        timestamp_table_livecount = pd.read_sql('select MAX(live_count) from sbms_stg.timestamp_table_test_env where unique_id = "{0}";'.format(u_id),con=srcDB_conn)
        timestamp_table_livecount = timestamp_table_livecount.values[0][0]
        print('Time taken to get the max live count from timestamp table: ',(datetime.now()-max_livecount_time).total_seconds())
        
        if timestamp_table_livecount != None and ((Objs_dict[u_id]['Obj_Description'].lastLiveCount == 0) or (timestamp_table_livecount>Objs_dict[u_id]['Obj_Description'].lastLiveCount)):
            Objs_dict[u_id]['Obj_Description'].lastLiveCount = timestamp_table_livecount
        
        print('Last Livecount: ',Objs_dict[u_id]['Obj_Description'].lastLiveCount)
        stg_get_time=datetime.now()
        staging_df = pd.read_sql('select unique_id,asset_token,live_count,temp_mobile_time,voltage,current from sbms_stg.module_rawdata_test_env where unique_id = "{0}" and live_count > {1} and cloudtime_modified = "{2}" and mobile_time IS NULL;'.format(u_id,Objs_dict[u_id]['Obj_Description'].lastLiveCount,'N'),con=srcDB_conn)
        print('Time taken to fetch the data for specific unique_id from stg table: ',(datetime.now()-stg_get_time).total_seconds())
        print('No. of records from of Staging df before sorting: ',staging_df.shape[0])
        duplicate_time=datetime.now()
        staging_df.drop_duplicates(subset ="live_count", keep ='first' , inplace = True)
        print('Time taken to delete duplicate records: ',(datetime.now()-duplicate_time).total_seconds())
        
        if(len(staging_df) < 2):
            print("No New Record for id: ",u_id)
            continue
        
        sort_time=datetime.now()
        staging_df.sort_values(by=['live_count'], ascending=True,inplace = True)
        staging_df.reset_index(drop=True,inplace = True)
        print('Time taken to sort and re-index records: ',(datetime.now()-sort_time).total_seconds())
        print('No. of records from of Staging df after sorting: ',staging_df.shape[0])
        
        process_time = datetime.now()
        try:
            timestamp_df = Objs_dict[u_id]['Obj_Description'].get_timestamp_df(staging_df)
        except Exception as e:
                print('Empty temp_mobile column in',srcDB_conn)
                continue
        print('Time taken to process timestamps: ',(datetime.now()-process_time).total_seconds())
        
        if (len(timestamp_df) == 0):
            print('No new Timestamp available for id: ',u_id)
            continue
        
        update_time = datetime.now()
        final_livecount = pushTimestamp(timestamp_df,u_id,srcDB_conn)  ## Timestamp Push
        Objs_dict[u_id]['Obj_Description'].lastLiveCount = final_livecount
        print('Time taken to push timestamps to table: ',(datetime.now()-update_time).total_seconds())
        
        udt2 = datetime.now()
        print('Overall Time Taken for '+ u_id +':',((udt2-udt1).total_seconds()))

    endtime = datetime.now()
    print('Time Taken for all uids:',((endtime-starttime).total_seconds()))
    print('Sleep for 1 Seconds')
    time.sleep(1)