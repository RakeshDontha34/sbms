"""
Created on Thu Dec  16 03:05:56 2020

@author: Kalyan,Hari_Ramaraju.

@version : V1.0
"""
import pandas as pd
import logging
import json
import os
import time
import warnings
warnings.filterwarnings('ignore')
from Src.DBConnect import DBConnect
from Src.ComputeFunctions import ObjectsCreation,pushRecordTOProduction,checkProd_RUL_MatchFailure
from Src.ComputeFunctions import getPrevious_Production_Values,updateIntermid_lookup,getPrevious_Intermid_lookup_Values


global sba_refer_lookup
#Headcoded values
sourceDB_connection = None
destinationDB_connection = None

log_location=os.getcwd()
logging.basicConfig(filename=log_location+'/sbms_auto.log', level=logging.DEBUG, 
                    format='%(asctime)s %(levelname)s %(name)s %(lineno)s %(message)s')
logger=logging.getLogger(__name__)
#DB Connect and get data
#Data base Session Creation

try:
    Obj_DB = DBConnect()
    sourceDB_connection = Obj_DB.getSourceDBconnection()
    destinationDB_connection = Obj_DB.getDestinationDBconnection() 
    
except Exception as e:
        print(' production database insertion failed for ',sourceDB_connection)
        logging.exception(' production database insertion failed for ',destinationDB_connection)
        print(e)
        logging.error('Error at %s', 'division', exc_info=e)
    
#Execution Start Point 

Objs_dict= {} 
#init params
sba_refer_lookup = pd.read_sql('select Cycle_Index,crank_resistance,SOH from sba_refer_lookup_test_env',con=destinationDB_connection)

while True:
    print('Batch Process Started...')
    pro_ids=pd.read_sql('select distinct unique_id from module_rawdata_test_env',con=sourceDB_connection)

    list_unique_ids=pro_ids['unique_id'].to_list()
    Objs_dict = ObjectsCreation(list_unique_ids,Objs_dict,sba_refer_lookup)
    
    #print('Objs_dict main ',Objs_dict)
    for get_id in list_unique_ids: 
        get_capacity=pd.read_sql('select designcapacity from auto_asset_master_test_env where uniqueid='+"'"+str(get_id)+"'",con=sourceDB_connection)
        #need not get sigmax,soc_kf,soh,crnk_soh,crank_info,rul,rul_std
        production_df = pd.read_sql('select unique_id,live_count,cloud_time,voltage,current_nc,temperature from a_module_status_dup_test_env where unique_id = '+"'"+str(get_id)+"' order by live_count desc limit 1",con=destinationDB_connection)
        #timestamp_df = pd.read_sql('select live_count,cloud_time from timestamp_table where unique_id = "{0}" and live_count={1}'.format(get_id,production_df1['live_count'][0]),con=destinationDB_connection)
        #production_df = pd.merge(production_df1,timestamp_df,on='live_count',how='inner')
        #RUL_lookup_df=pd.read_sql('select unique_id,live_count,past_data,past_data_index,initial_popt,initial_pcov from rul_lookup where unique_id = '+"'"+str(get_id)+"';",con=destinationDB_connection)
        intermid_lookup_df =pd.read_sql('select unique_id,live_count,cloud_time,soc_kf,soh,rul,rul_std,sigmax,crnk_soh,crank_info,past_data,past_data_index,initial_popt,initial_pcov from intermid_values_lookup_test_env where unique_id = '+"'"+str(get_id)+"';",con=destinationDB_connection)

        #assume_live_count is used to get new data from Staging 
        if len(production_df)==0:
            assume_live_count=0
        else:
            assume_live_count=production_df['live_count'][0]
        
        if len(get_capacity)==0:
           capacity=65
        else:
            capacity=int(get_capacity['designcapacity'][0])
        #new_uniqueid_flag is used to check if inertion/replacement needed in intermid_values_lookup_test_env table.
        if len(intermid_lookup_df)==0:
            UID_init_flag='Y'
        else:            
            UID_init_flag='N'
        #Checking Failure Condition when Production data nad RUL_Lookup is not matched.
        if (len(intermid_lookup_df)>0 and len(production_df)>0):
            if (intermid_lookup_df['live_count'][0] < production_df['live_count'][0]):
                
                checkProd_RUL_df = pd.read_sql('select unique_id,live_count,cloud_time,voltage,current_nc from a_module_status_dup_test_env where unique_id = '+"'"+str(get_id)+"'"+" and live_count > "+"'"+str(intermid_lookup_df['live_count'][0]-1)+"'",con=destinationDB_connection)
                checkProd_RUL_MatchFailure(UID_init_flag,get_id,capacity,Objs_dict,checkProd_RUL_df,intermid_lookup_df,sba_refer_lookup,destinationDB_connection)
                continue
        elif (len(intermid_lookup_df) == 0 and len(production_df)>0):
                checkProd_RUL_df = pd.read_sql('select unique_id,live_count,cloud_time,voltage,current_nc from a_module_status_dup_test_env where unique_id = '+"'"+str(get_id)+"'",con=destinationDB_connection)
                checkProd_RUL_MatchFailure(UID_init_flag,get_id,capacity,Objs_dict,checkProd_RUL_df,intermid_lookup_df,sba_refer_lookup,destinationDB_connection)
                continue
        #Get data from Staging Table/Server.
        timestamp_df2 = pd.read_sql('select live_count,cloud_time from timestamp_table where unique_id = "{0}" and live_count > {1}'.format(get_id,assume_live_count),con=sourceDB_connection)
        if len(timestamp_df2)>1:
            min_tt = min(timestamp_df2['live_count'])
            max_tt = max(timestamp_df2['live_count'])
        else:
            continue
        stgDB_data1=pd.read_sql('select unique_id,live_count,asset_token,voltage,current,temperature,mobile_time from module_rawdata_test_env where unique_id = "{0}" and cloudtime_modified = "{1}" and live_count between {2} and {3}'.format(get_id,'N',min_tt,max_tt),con=sourceDB_connection)
        stgDB_data = pd.merge(stgDB_data1,timestamp_df2,on='live_count',how='inner')
        stgDB_data_len = len(stgDB_data)
        
        
        if(len(production_df)>0):                    
            Objs_dict[get_id]["SOH"].prev_volt,Objs_dict[get_id]["SOH"].prev_curr = getPrevious_Production_Values(production_df)
        if(len(intermid_lookup_df) > 0):          
            Objs_dict[get_id]["SOC"].previous_soc_kf,Objs_dict[get_id]["SOC"].previous_sigmax,Objs_dict[get_id]["SOH"].prev_soh,Objs_dict[get_id]["SOH"].crnk_soh,Objs_dict[get_id]["SOH"].crank_info,Objs_dict[get_id]["RUL"].rul_mean,Objs_dict[get_id]["RUL"].rul_std,Objs_dict[get_id]["RUL"].past_data,Objs_dict[get_id]["RUL"].past_data_index,Objs_dict[get_id]["RUL"].initial_popt,Objs_dict[get_id]["RUL"].initial_pcov = getPrevious_Intermid_lookup_Values(intermid_lookup_df)
        
        if stgDB_data_len>0:
            stgDB_data.drop_duplicates(subset ="live_count", keep ='first' , inplace = True)
            stgDB_data.sort_values(by=['live_count'], ascending=True,inplace = True)
            stgDB_data.reset_index(drop=True,inplace = True)
            
            production_push_df = pd.DataFrame()
            for i in range(stgDB_data_len):
                voltage=stgDB_data['voltage'][i]
                current_org=stgDB_data['current'][i]
                temperature=stgDB_data['temperature'][i]
                livecount=stgDB_data['live_count'][i]
                uniqueid=stgDB_data['unique_id'][0]
                assettoken=stgDB_data['asset_token'][0]
                mobile_time=str(stgDB_data['cloud_time'][i])
                cloud_time=str(stgDB_data['cloud_time'][i])
                current=current_org
                current=Objs_dict[get_id]["SOC"].remove_noise(current)
                step_time=Objs_dict[get_id]["SOC"].get_step_time(current)
                if (Objs_dict[get_id]["SOC"].init_soc_count==0):
                    Objs_dict[get_id]["SOC"].previous_soc_kf = Objs_dict[get_id]["SOC"].get_soc_ini(voltage,sourceDB_connection)
                    Objs_dict[get_id]["SOC"].init_soc_count=1
                
                Objs_dict[get_id]["SOC"].previous_soc_kf,Objs_dict[get_id]["SOC"].previous_sigmax = Objs_dict[get_id]["SOC"].kalman_filter(Objs_dict[get_id]["SOC"].previous_soc_kf,Objs_dict[get_id]["SOC"].previous_sigmax,voltage,current,step_time,capacity)
                
                Objs_dict[get_id]["SOH"].roll_soh,Objs_dict[get_id]["SOH"].crnk_soh,Objs_dict[get_id]["SOH"].crank_info = Objs_dict[get_id]["SOH"].get_soh(current,voltage,Objs_dict[get_id]["SOH"].prev_volt,Objs_dict[get_id]["SOH"].prev_curr,Objs_dict[get_id]["SOH"].prev_soh,Objs_dict[get_id]["SOH"].crnk_soh,Objs_dict[get_id]["SOH"].crank_info)
                soh=float(Objs_dict[get_id]["SOH"].roll_soh)*100
                
                Objs_dict[get_id]["SOH"].prev_soh,Objs_dict[get_id]["RUL"].rul_mean,Objs_dict[get_id]["RUL"].rul_std,Objs_dict[get_id]["RUL"].past_data,Objs_dict[get_id]["RUL"].past_data_index,Objs_dict[get_id]["RUL"].initial_popt,Objs_dict[get_id]["RUL"].initial_pcov = Objs_dict[get_id]["RUL"].get_rul(Objs_dict[get_id]["SOH"].prev_soh,Objs_dict[get_id]["SOH"].roll_soh,Objs_dict[get_id]["RUL"].past_data,Objs_dict[get_id]["RUL"].past_data_index,Objs_dict[get_id]["RUL"].rul_mean,Objs_dict[get_id]["RUL"].rul_std,Objs_dict[get_id]["RUL"].initial_popt,Objs_dict[get_id]["RUL"].initial_pcov)
                Objs_dict[get_id]["SOH"].prev_volt = voltage
                Objs_dict[get_id]["SOH"].prev_curr = current
                
                Objs_dict[get_id]["Asset_Info"].soc_status=Objs_dict[get_id]["Asset_Info"].getSoC_Status(Objs_dict[get_id]["SOC"].previous_soc_kf)
                Objs_dict[get_id]["Asset_Info"].soh_status=Objs_dict[get_id]["Asset_Info"].getSoH_Status(soh)
                #print('Reached Upto here...')
                print('livecount  ',livecount,"get_id :",get_id,"SOC:", Objs_dict[get_id]["SOC"].previous_soc_kf,'SOH:',soh,'RUL:',Objs_dict[get_id]["RUL"].rul_mean,'\n')
                
    
                #replace mobile_time with cloud_time
                
                production_push_sub_df=pd.DataFrame({'unique_id':uniqueid,'asset_token':assettoken,'live_count':livecount,'soc_kf':format(float( Objs_dict[get_id]["SOC"].previous_soc_kf),'.2f'),'soh':format(float(soh),'.2f'),'rul':Objs_dict[get_id]["RUL"].rul_mean,'rul_std':Objs_dict[get_id]["RUL"].rul_std,'voltage':voltage, 'current':current_org,'current_nc':format(current,'.2f'),'temperature':temperature,'module_status':Objs_dict[get_id]["Asset_Info"].soh_status,'module_capacity':str(capacity),'server_time':str(cloud_time),'soc_status':Objs_dict[get_id]["Asset_Info"].soc_status,'soh_status':Objs_dict[get_id]["Asset_Info"].soh_status,'cloud_time':str(cloud_time),'descriptive_processed_flag':0},index = [0])
                production_push_df = pd.concat([production_push_df,production_push_sub_df],axis=0)
                production_push_df.reset_index(inplace= True, drop= True)
                
                
            pushRecordTOProduction(production_push_df,destinationDB_connection,get_id,livecount)
            
            #replace mobile_time with cloud_time
            #if(i == (stgDB_data_len -1)):
            min_rul = Objs_dict[get_id]["RUL"].rul_mean - Objs_dict[get_id]["RUL"].rul_std
            max_rul = Objs_dict[get_id]["RUL"].rul_mean + Objs_dict[get_id]["RUL"].rul_std
            #interMidLookup_data=pd.DataFrame.from_dict({'unique_id':[uniqueid],'live_count':[livecount],'cloud_time':str(cloud_time),'soc_kf':format(float( Objs_dict[get_id]["SOC"].previous_soc_kf),'.2f'),'soh':format(float(soh),'.2f'),'rul':Objs_dict[get_id]["RUL"].rul_mean,'min_rul': min_rul,'max_rul':max_rul,'rul_std':Objs_dict[get_id]["RUL"].rul_std,'sigmax':format(float(Objs_dict[get_id]["SOC"].previous_sigmax),'.4f'),'crnk_soh':str(Objs_dict[get_id]["SOH"].crnk_soh),'crank_info':str(Objs_dict[get_id]["SOH"].crank_info),'past_data':[Objs_dict[get_id]["RUL"].past_data.to_dict('records')],'past_data_index':[Objs_dict[get_id]["RUL"].past_data_index],'initial_popt':str(Objs_dict[get_id]["RUL"].initial_popt),'initial_pcov':str(Objs_dict[get_id]["RUL"].initial_pcov)})
            interMidLookup_data=pd.DataFrame({'unique_id':uniqueid,'live_count':livecount,'cloud_time':str(cloud_time),'soc_kf':format(float( Objs_dict[get_id]["SOC"].previous_soc_kf),'.2f'),'soh':format(float(soh),'.2f'),'rul':Objs_dict[get_id]["RUL"].rul_mean,'min_rul': min_rul,'max_rul':max_rul,'rul_std':Objs_dict[get_id]["RUL"].rul_std,'sigmax':format(float(Objs_dict[get_id]["SOC"].previous_sigmax),'.4f'),'crnk_soh':str(Objs_dict[get_id]["SOH"].crnk_soh),'crank_info':json.dumps(Objs_dict[get_id]["SOH"].crank_info),'past_data':[Objs_dict[get_id]["RUL"].past_data.to_json(orient = 'records')],'past_data_index':Objs_dict[get_id]["RUL"].past_data_index,'initial_popt':str(Objs_dict[get_id]["RUL"].initial_popt),'initial_pcov':str(Objs_dict[get_id]["RUL"].initial_pcov)},index=[0])
            updateIntermid_lookup(UID_init_flag,get_id,livecount,interMidLookup_data,destinationDB_connection)
            
            
    time.sleep(1)





