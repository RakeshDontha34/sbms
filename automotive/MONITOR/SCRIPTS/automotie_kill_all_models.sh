:%s/\r$//
:x
##########
#DESCRIPTION: Script to KILL ALL EXISTING PROCESSES (PYTHON)
#WRITTEN: Siva Kumar
#CREATED_DATE: 8-3-2021
#SCRIPT: /bin/sh /home/azureuser/sbms/lead_acid/automotive/MONITOR/SCRIPTS/KILL_PID_BEFORE_SCRIPTS_LAUNCH_v1.sh 1>/home/azureuser/sbms/lead_acid/automotive/MONITOR/LOGS/KILL_PID_BEFORE_SCRIPTS_LAUNCH_v1.txt 2>&1
#UPDATED DATE/PERSON/REASON:
##################################
##################################

Script_path="/home/azureuser/sbms/lead_acid/automotive/MONITOR/LOGS/TEMP_FILES"

#python scripts 
script_1="stage1_ETL_job"
script_2="cloud_timestamp_processor"
script_3="StartBMS_App"
script_4="StartBMS_Descriptives"
script_5="StartBMS_Crank_Descriptives"
script_6="TEST"

echo "shell script started at:"
p_RunDate=`date +"%Y-%m-%d %H:%M:%S"`
echo "RunDate is ${p_RunDate}"


#####################to get the PID#########
script_1_pid=`ps -eaf|grep -m1 ${script_1}`
script_2_pid=`ps -eaf|grep -m1 ${script_2}`
script_3_pid=`ps -eaf|grep -m1 ${script_3}`
script_4_pid=`ps -eaf|grep -m1 ${script_4}`
script_5_pid=`ps -eaf|grep -m1 ${script_5}`
script_6_pid=`ps -eaf|grep -m1 ${script_6}`

##################### PID into a temp_file#########

echo "${script_1_pid}" > ${Script_path}/pid_1.txt
echo "${script_2_pid}" > ${Script_path}/pid_2.txt 
echo "${script_3_pid}" > ${Script_path}/pid_3.txt 
echo "${script_4_pid}" > ${Script_path}/pid_4.txt 
echo "${script_5_pid}" > ${Script_path}/pid_5.txt 
echo "${script_6_pid}" > ${Script_path}/pid_6.txt 

################	 PID into a file with kill command assigned to it ##### 	 KILL ANY EXISTING PROCESSESS ARE RUNNING 	####

cut -b 10-16 ${Script_path}/pid_1.txt > ${Script_path}/${script_1}_pid.sh
sed -i -e 's/^/kill /' ${Script_path}/${script_1}_pid.sh
#/bin/sh ${Script_path}/${script_1}_pid.sh

cut -b 10-16 ${Script_path}/pid_2.txt > ${Script_path}/${script_2}_pid.sh
sed -i -e 's/^/kill /' ${Script_path}/${script_2}_pid.sh
#/bin/sh ${Script_path}/${script_2}_pid.sh

cut -b 10-16 ${Script_path}/pid_3.txt > ${Script_path}/${script_3}_pid.sh
sed -i -e 's/^/kill /' ${Script_path}/${script_3}_pid.sh
#/bin/sh ${Script_path}/${script_3}_pid.sh

cut -b 10-16 ${Script_path}/pid_4.txt > ${Script_path}/${script_4}_pid.sh
sed -i -e 's/^/kill /' ${Script_path}/${script_4}_pid.sh
#/bin/sh ${Script_path}/${script_4}_pid.sh

cut -b 10-16 ${Script_path}/pid_5.txt > ${Script_path}/${script_5}_pid.sh
sed -i -e 's/^/kill /' ${Script_path}/${script_5}_pid.sh
#/bin/sh ${Script_path}/${script_5}_pid.sh

cut -b 10-16 ${Script_path}/pid_6.txt > ${Script_path}/${script_6}_pid.sh
sed -i -e 's/^/kill /' ${Script_path}/${script_6}_pid.sh


##################################
echo "end of script execution"
##echo `date -d 'today' +%Y-%m-%d`
`date +"%Y-%m-%d %H:%M:%S"`