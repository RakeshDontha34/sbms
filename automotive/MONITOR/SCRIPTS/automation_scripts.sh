:%s/\r$//
:x
##########
#DESCRIPTION: Script to launch existing 5 jobs concerantly and get the PID for the scripts
#WRITTEN: Siva Kumar
#CREATED_DATE: 8-3-2021
#SCRIPT: /bin/sh /home/azureuser/automotive/Script_Automation_test/sample_shell_automation_scripts.sh 1>/home/azureuser/automotive/Script_Automation_test/sample_shell_automation_scripts.tct 2>&1
#UPDATED DATE/PERSON/REASON:
##################################
##################################

Script_path="/home/azureuser/sbms/lead_acid/automotive"

script_1="stage1_ETL_job"
script_2="cloud_timestamp_processor"
script_3="StartBMS_App"
script_4="StartBMS_Descriptives"
script_5="StartBMS_Crank_Descriptives"

echo "shell script started at:"
p_RunDate=date +"%Y-%m-%d %H:%M:%S"
echo "RunDate is ${p_RunDate}"


#python3 ${Script_path}/tets_script_1.py > ${Script_path}/tets_script_1_log.txt 2>&1 &
#parallel ::: "nohup python3 /home/azureuser/automotive/Script_Automation_test/tets_script_1.py > /home/azureuser/automotive/Script_Automation_test/tets_script_1.txt" "nohup python3 /home/azureuser/automotive/Script_Automation_test/tets_script_2.py > /home/azureuser/automotive/Script_Automation_test/tets_script_2.txt"

##########Script-1#########
echo "${script_1 execution starting at"
date +"%Y-%m-%d %H:%M:%S" 
nohup python3 /home/azureuser/sbms/lead_acid/automotive/ETL/${script_1} > /home/azureuser/sbms/lead_acid/automotive/ETL/${script_1}.txt & disown


##########Script-2#########
echo "${script_2} execution starting at"
date +"%Y-%m-%d %H:%M:%S"
nohup python3 /home/azureuser/sbms/lead_acid/automotive/ETL/${script_2} > /home/azureuser/sbms/lead_acid/automotive/ETL/${script_2}.txt & disown

##########Script-3#########
echo "${script_3} execution starting at"
date +"%Y-%m-%d %H:%M:%S"
nohup python3 /home/azureuser/sbms/lead_acid/automotive/${script_3} > /home/azureuser/sbms/lead_acid/automotive/${script_3}.txt & disown

##########Script-4#########
echo "${script_4} execution starting at"
date +"%Y-%m-%d %H:%M:%S"
nohup python3 /home/azureuser/sbms/lead_acid/automotive/${script_4} > /home/azureuser/sbms/lead_acid/automotive/${script_4}.txt & disown

##########Script-5#########
echo "${script_5} execution starting at"
date +"%Y-%m-%d %H:%M:%S"
nohup python3 /home/azureuser/sbms/lead_acid/automotive/${script_5} > /home/azureuser/sbms/lead_acid/automotive/${script_5}.txt & disown


#####################to get the PID#########
script_1_pid=`ps -eaf|grep -i ${script_1}`
script_2_pid=`ps -eaf|grep -i ${script_2}`
script_3_pid=`ps -eaf|grep -i ${script_3}`
script_4_pid=`ps -eaf|grep -i ${script_4}`
script_5_pid=`ps -eaf|grep -i ${script_5}`

##################### PID into a temp_file#########
echo "${script_1_pid}" > /home/azureuser/sbms/lead_acid/automotive/ETL/pid_1.txt
echo "${script_2_pid}" > /home/azureuser/sbms/lead_acid/automotive/ETL/pid_2.txt 
echo "${script_3_pid}" > /home/azureuser/sbms/lead_acid/automotive/pid_3.txt 
echo "${script_4_pid}" > /home/azureuser/sbms/lead_acid/automotive/pid_4.txt 
echo "${script_5_pid}" > /home/azureuser/sbms/lead_acid/automotive/pid_5.txt 

##################### PID into a file with kill command assigned to it #########
cut -b 10-16 /home/azureuser/automotive/Script_Automation_test/pid_1.txt > /home/azureuser/automotive/Script_Automation_test/${script_1}_pid.txt
sed -i -e 's/^/kill /' /home/azureuser/automotive/Script_Automation_test/${script_1}_pid.txt

cut -b 10-16 /home/azureuser/automotive/Script_Automation_test/pid_1.txt > /home/azureuser/automotive/Script_Automation_test/${script_2}_pid.txt
sed -i -e 's/^/kill /' /home/azureuser/automotive/Script_Automation_test/${script_2}_pid.txt

cut -b 10-16 /home/azureuser/automotive/Script_Automation_test/pid_1.txt > /home/azureuser/automotive/Script_Automation_test/${script_3}_pid.txt
sed -i -e 's/^/kill /' /home/azureuser/automotive/Script_Automation_test/${script_3}_pid.txt

cut -b 10-16 /home/azureuser/automotive/Script_Automation_test/pid_1.txt > /home/azureuser/automotive/Script_Automation_test/${script_4}_pid.txt
sed -i -e 's/^/kill /' /home/azureuser/automotive/Script_Automation_test/${script_4}_pid.txt

cut -b 10-16 /home/azureuser/automotive/Script_Automation_test/pid_1.txt > /home/azureuser/automotive/Script_Automation_test/${script_5}_pid.txt
sed -i -e 's/^/kill /' /home/azureuser/automotive/Script_Automation_test/${script_5}_pid.txt


##################################
echo "end of script execution"
##echo `date -d 'today' +%Y-%m-%d`
date +"%Y-%m-%d %H:%M:%S