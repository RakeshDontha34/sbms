##########
#DESCRIPTION: Script to launch python jobs by following below steps
# 1. kill any existing python jobs
# 2. launch Python jobs
# 3. check if all the python jobs are up & running	
#WRITTEN: Siva Kumar
#CREATED_DATE: 8-3-2021
#SCRIPT: /bin/sh /home/azureuser/automotive/Script_Automation_test/sbms_automotive_scrips.sh 1>/home/azureuser/automotive/Script_Automation_test/sbms_automotive_scrips.txt 2>&1
#UPDATED DATE/PERSON/REASON:
##################################
##################################

##Script_path="/home/azureuser/sbms/lead_acid/automotive/MONITOR/SCRIPTS"
Script_path1="/home/azureuser/stationary/drista_bms_table_script"
dristia_bms_module_script.py

Log_path="/home/azureuser/sbms/lead_acid/automotive/MONITOR/LOGS"

script_1="stage1_ETL_job"
script_2="cloud_timestamp_processor"
script_3="StartBMS_App"
script_4="StartBMS_Descriptives"
script_5="StartBMS_Crank_Descriptives"

echo "shell script started at:"
p_RunDate=`date +"%Y-%m-%d %H:%M:%S"`
echo "RunDate is ${p_RunDate}"

echo "kill all existing processess before starting - launch KILL_PID_BEFORE_SCRIPTS_LAUNCH.sh script"
/bin/sh /home/azureuser/sbms/lead_acid/automotive/MONITOR/LOGS/KILL_PID_BEFORE_SCRIPTS_LAUNCH.sh 1> ${Log_path}/KILL_PID_BEFORE_SCRIPTS_LAUNCH.txt 2>&1

##########Script-1#########
echo "${script_1} execution starting at"
date +"%Y-%m-%d %H:%M:%S" 
nohup python3 /home/azureuser/sbms/lead_acid/automotive/ETL/${script_1}.py > ${Log_path}/${script_1}_${p_RunDate}.txt & disown
sleep 2m
#check pid exist or not

##########Script-2#########
echo "${script_2} execution starting at"
date +"%Y-%m-%d %H:%M:%S"
nohup python3 /home/azureuser/sbms/lead_acid/automotive/ETL/${script_2}.py > ${Log_path}/${script_2}_${p_RunDate}.txt & disown
sleep 1m
##########Script-3#########
echo "${script_3} execution starting at"
date +"%Y-%m-%d %H:%M:%S"
nohup python3 /home/azureuser/sbms/lead_acid/automotive/MODEL/${script_3}.py > ${Log_path}/${script_3}_${p_RunDate}.txt & disown
sleep 1m
##########Script-4#########
echo "${script_4} execution starting at"
date +"%Y-%m-%d %H:%M:%S"
nohup python3 /home/azureuser/sbms/lead_acid/automotive/MODEL/${script_4}.py > ${Log_path}/${script_4}_${p_RunDate}.txt & disown
sleep 1m
##########Script-5#########
echo "${script_5} execution starting at"
date +"%Y-%m-%d %H:%M:%S"
nohup python3 /home/azureuser/sbms/lead_acid/automotive/MODEL/${script_5}.py > ${Log_path}/${script_5}_${p_RunDate}.txt & disown


#####################to get the PID#########
#script_1_pid=`ps -eaf|grep -i ${script_1}`
#script_2_pid=`ps -eaf|grep -i ${script_2}`
#script_3_pid=`ps -eaf|grep -i ${script_3}`
#script_4_pid=`ps -eaf|grep -i ${script_4}`
#script_5_pid=`ps -eaf|grep -i ${script_5}`

##################### PID into a temp_file#########
#echo "${script_1_pid}" > /home/azureuser/automotive/Script_Automation_test/pid_1.txt
#echo "${script_2_pid}" > /home/azureuser/automotive/Script_Automation_test/pid_2.txt 
#echo "${script_3_pid}" > /home/azureuser/automotive/Script_Automation_test/pid_3.txt 
#echo "${script_4_pid}" > /home/azureuser/automotive/Script_Automation_test/pid_4.txt 
#echo "${script_5_pid}" > /home/azureuser/automotive/Script_Automation_test/pid_5.txt 

##################### PID into a file with kill command assigned to it #########
#cut -b 10-16 /home/azureuser/automotive/Script_Automation_test/pid_1.txt > /home/azureuser/automotive/Script_Automation_test/${script_1}_pid.txt
#sed -i -e 's/^/kill /' /home/azureuser/automotive/Script_Automation_test/${script_1}_pid.txt

#cut -b 10-16 /home/azureuser/automotive/Script_Automation_test/pid_1.txt > /home/azureuser/automotive/Script_Automation_test/${script_2}_pid.txt
#sed -i -e 's/^/kill /' /home/azureuser/automotive/Script_Automation_test/${script_2}_pid.txt

#cut -b 10-16 /home/azureuser/automotive/Script_Automation_test/pid_1.txt > /home/azureuser/automotive/Script_Automation_test/${script_3}_pid.txt
#sed -i -e 's/^/kill /' /home/azureuser/automotive/Script_Automation_test/${script_3}_pid.txt

#cut -b 10-16 /home/azureuser/automotive/Script_Automation_test/pid_1.txt > /home/azureuser/automotive/Script_Automation_test/${script_4}_pid.txt
#sed -i -e 's/^/kill /' /home/azureuser/automotive/Script_Automation_test/${script_4}_pid.txt

#cut -b 10-16 /home/azureuser/automotive/Script_Automation_test/pid_1.txt > /home/azureuser/automotive/Script_Automation_test/${script_5}_pid.txt
#sed -i -e 's/^/kill /' /home/azureuser/automotive/Script_Automation_test/${script_5}_pid.txt

#########################################################################
###########check if all the scripts are running or not######

#/bin/sh /home/azureuser/automotive/Script_Automation_test/PID_check.sh 1>/home/azureuser/automotive/Script_Automation_test/PID_check_${p_RunDate}.txt 2>&1


##################################
echo "end of script execution"
##echo `date -d 'today' +%Y-%m-%d`
date +"%Y-%m-%d %H:%M:%S"