from sqlalchemy import create_engine
import pymysql
import pandas as pd
import time
import numpy as np
import sqlalchemy
import pickle
from datetime import datetime,date
from datetime import timedelta
import pymysql
import pymysql.cursors
import warnings
warnings.filterwarnings("ignore")



#unique_ids=['5fce621084a4a13ab34e21c9']
### source database connection (SBMS) ######
database_username_source = 'root'
database_password_source = 'Root@123'
database_ip_source      = '10.0.0.5'
database_name_source     = 'sbms_stg'
database_connection_source = sqlalchemy.create_engine('mysql+mysqlconnector://{0}:{1}@{2}/{3}'.
                                               format(database_username_source, database_password_source, 
                                                      database_ip_source, database_name_source))
                                                      

### destination database connection - to push the data into respective table  ######
database_username_destination = 'root'
database_password_destination = 'Root@123'
database_ip_destination    = '10.0.0.5'
database_name_destination    = 'sbms_stg'
database_connection_destination = sqlalchemy.create_engine('mysql+mysqlconnector://{0}:{1}@{2}/{3}'.
                                               format(database_username_destination, database_password_destination, 
                                                      database_ip_destination, database_name_destination))



while True:
    pro_ids=pd.read_sql('select distinct unique_id from module_rawdata where cloud_time is null',con=database_connection_source)
    unique_ids=pro_ids['unique_id'].to_list()
    if len(unique_ids)>0:
        try:  
            for get_id in unique_ids:
                pro_data=pd.read_sql('select unique_id, live_count, current, mobile_time, cloud_time,last_non_crank from module_rawdata where unique_id = '+"'"+str(get_id)+"'"+' and cloud_time is null and processed = '+"'"+str('YES')+"'"+"  order by live_count asc ",con=database_connection_source)
                last_record=pd.read_sql('select unique_id, live_count, current, mobile_time, cloud_time,last_non_crank from module_rawdata where unique_id = '+"'"+str(get_id)+"'"+' and cloud_time is not null and processed = '+"'"+str('YES')+"'"+"  order by live_count desc limit 1 ",con=database_connection_source)
                #print(get_id)
    
                if len(last_record)==0:
                    #print("last_record=0")
                    start_record=1
                    pro_data.mobile_time = pd.to_datetime(pro_data.mobile_time)
                    pro_data["cloud_time"].iloc[0] =pro_data.mobile_time[0]
                    pro_data.last_non_crank.iloc[0] = pro_data.mobile_time[0]
                    last_timestamp =pd.to_datetime(pro_data.mobile_time[0])
                    last_non_crank = pd.to_datetime(pro_data.mobile_time[0])
                    previous_current=pro_data.current[0]
                else:
                    #print("last_record!=0")
                    start_record=0
                    #pro_data.mobile_time = pd.to_datetime(last_record.cloud_time[0] )
                    last_timestamp =pd.to_datetime(last_record.cloud_time[0])
                    if(last_record.last_non_crank[0]==None or last_record.last_non_crank[0]=='NULL'):
                        #print("last_non_crank=null")
                        last_non_crank = pd.to_datetime(last_record.cloud_time[0])
                        #print("last_non_crank",last_record.cloud_time[0])
                    else:
                        #print("last_non_crank!=0")
                        last_non_crank = pd.to_datetime(last_record.last_non_crank[0])
                    #last_non_crank = pd.to_datetime(last_record.last_non_crank[0])
                    previous_current=last_record.current[0]
                    
                if len(pro_data)>0:
                    #print("pro_data >=0")
                    pro_data.drop_duplicates(subset=['live_count'],keep='first',inplace=True)
                    pro_data.sort_values(by=['live_count'], ascending=True, inplace=True)
                    pro_data.reset_index(drop=True,inplace=True)
                    
                data=pro_data.copy()
                #print('id ',get_id, ' count ',len(data))
                for i in range(start_record,len(data)):
                    #print(i)
                    if i>0:
                        previous_current=data.current.iloc[i-1]
                    if(abs(data.current.iloc[i]) < 100 ):
                        #print(data.current.iloc[i])
                        if(abs(previous_current) > 100):
                            data["cloud_time"].iloc[i] = last_non_crank + timedelta(minutes=1)
                           # print("previous_current > 100 cloud_time",data["cloud_time"].iloc[i])
                        else:
                            data["cloud_time"].iloc[i] = last_timestamp + timedelta(minutes=1)
                            #print("previous_current < 100 cloud_time",data["cloud_time"].iloc[i])
                        last_non_crank = data.cloud_time.iloc[i]
                        last_timestamp=data.cloud_time.iloc[i]
                        data['last_non_crank'].iloc[i]=data["cloud_time"].iloc[i]
                        print("current < 0 last_non_crank",data['last_non_crank'].iloc[i])
                    else:
                        data["cloud_time"].iloc[i] = last_timestamp + timedelta(milliseconds=30)
                        #print("current > 100 cloud_time",data["cloud_time"].iloc[i])
                        #print("current > 100 last_non_crank",last_non_crank)
                        diff = data["cloud_time"].iloc[i]- last_non_crank
                        #print(diff.microseconds/1000)
                        if(diff.microseconds>55000000):
                            data["cloud_time"].iloc[i] = last_timestamp
                        last_timestamp=data["cloud_time"].iloc[i]
                        data['last_non_crank'].iloc[i]=last_non_crank
                        
                #print("cloud_time",data["cloud_time"].iloc[i],"last_non_crank",data['last_non_crank'].iloc[i])
                try:
                    connection = pymysql.connect(host=database_ip_destination,user=database_username_destination,password=database_password_destination,db=database_name_destination,cursorclass=pymysql.cursors.DictCursor)
                    with connection.cursor() as cursor:
                        for j in range(len(data)):
                            #upt='update module_rawdata_new set cloud_time = '+"'"+str(data['TimeStamp'][j])+"'"+",cloudtime_modified="+"'"+str('Y')+"'"+" where unique_id = "+"'"+str(data['unique_id'][j])+"'"+" and live_count ="+"'"+str(data['live_count'][j])+"'"
                            upt='update module_rawdata set cloud_time = '+"'"+str(data['cloud_time'][j])+"'"+",cloudtime_modified="+"'"+str('Y')+"',"+"last_non_crank = "+"'"+str(data['last_non_crank'][j])+"'"+" where unique_id = "+"'"+str(data['unique_id'][j])+"'"+" and live_count ="+"'"+str(data['live_count'][j])+"'"
                            #print(upt)
                            cursor.execute(upt)
                            connection.commit()
                except Exception as ue:
                    print(ue)
                finally:
                    connection.close()
        except Exception as e:
            print(e)         
                      