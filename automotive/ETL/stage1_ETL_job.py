"""
Created on Fri Jan  8 14:33:05 2021
script 2
@author: Ram Prasad

"""
import datetime
import sqlalchemy
import pandas as pd
import time

performing_table='module_rawdata'

default_OCV_volatge=12.6
default_current=0
default_temperature=28
positive_cranking_threshold=39
negative_cranking_threshold=-39

header='#S'
ack='NACK'
is_live='n'
steptime=0
run_frequency=60 #seconds

database_username_destination = 'root'
database_password_destination = 'Root@123'
database_ip_destination    = '10.0.0.5'
database_name_destination    = 'sbms_stg'
database_connection_destination = sqlalchemy.create_engine('mysql+mysqlconnector://{0}:{1}@{2}/{3}'.
                                               format(database_username_destination, database_password_destination, 
                                                      database_ip_destination, database_name_destination))

def find_event(get_current):
    if float(get_current)==0:
        new_event='N'
    elif float(get_current) >0:
        new_event='C'
    else:
        new_event='D'
    return new_event


def current_cranking(value):
    if float(value) > positive_cranking_threshold:
        status='C'
    elif float(value) < negative_cranking_threshold:
        status='C'
    else:
        status="NC"
    return status

def delete_duplicate_for_uniqueid(u_id):
    ddp="""delete from  """+ performing_table+"""  where id in (
select id from (select id,unique_id, live_count, row_num from (
SELECT id,unique_id, live_count, 
ROW_NUMBER() OVER ( PARTITION BY live_count,unique_id ORDER BY live_count,unique_id ) AS row_num
FROM """ +performing_table+ """ where unique_id = """+"'"+str(u_id)+"'"+" ) dds  where row_num > 1) sdf)"
    return ddp


def check_latest_processed_livecount(df):
    if len(df)==0 or df['last_processed_livecount'][0]==None:
        processed_livecount=1
    else:
        processed_livecount=df['last_processed_livecount'][0]
    return int(processed_livecount)


event=find_event(default_current)
current_status=find_event(default_current)

#print(' script starting time ',datetime.datetime.now())
while True:
    pro_ids=pd.read_sql('select distinct unique_id from '+str(performing_table),con=database_connection_destination)
    list_unique_ids=pro_ids['unique_id'].to_list()
    #print(' All unique ids ',list_unique_ids)
    for u_id in list_unique_ids:
        delete_duplicates=delete_duplicate_for_uniqueid(u_id)
        database_connection_destination.execute(delete_duplicates)
        data=pd.read_sql('select unique_id,asset_token,live_count from '+str(performing_table)+' where unique_id = '+"'"+str(u_id)+"'"+" and processed = "+"'"+str("NO")+"'",con=database_connection_destination)
        last_processed_livecount=pd.read_sql('select max(live_count) as last_processed_livecount from '+str(performing_table)+' where unique_id = '+"'"+str(u_id)+"'"+" and processed = "+"'"+str("YES")+"'",con=database_connection_destination)
        last_processed=check_latest_processed_livecount(last_processed_livecount)
        if len(data)>0:
            #print(' unique id ',u_id,'total  records  ',len(data))
            live_counts=data['live_count'].to_list()
            if last_processed!=1:
                live_counts.append(last_processed)
                last_processed=int(last_processed)+1
            live_counts.sort()
            missing_live_counts=[x for x in range(last_processed, live_counts[-1]) if x not in live_counts]
            #print(' unique id ',u_id,' missing  ',missing_live_counts)
            #print(' last record processed for',u_id,' is ',last_processed)
            uniqueid=data['unique_id'][0]
            assettoken=data['asset_token'][0]
            if len(missing_live_counts)>0:
                for missed_livecount in missing_live_counts:
                    insert_stmt=" insert into "+str(performing_table)+" (header,ack,live_count,event,is_live,unique_id,asset_token,voltage,current,current_status,temperature,step_time) values ( "+"'"+str(header)+"',"+"'"+str(ack)+"',"+"'"+str(missed_livecount)+"',"+"'"+str(event)+"',"+"'"+str(is_live)+"',"+"'"+str(uniqueid)+"',"+"'"+str(assettoken)+"',"+"'"+str(default_OCV_volatge)+"',"+"'"+str(default_current)+"',"+"'"+str(current_status)+"',"+"'"+str(default_temperature)+"',"+"'"+str(steptime)+"')"
                    database_connection_destination.execute(insert_stmt)
            

            update_process=' update '+str(performing_table)+'  set processed = '+"'"+str('YES')+"'"+" where unique_id = "+"'"+str(uniqueid)+"' and live_count >= "+str(int(last_processed)-1)+" and live_count <= "+str(live_counts[-1])
            database_connection_destination.execute(update_process)
            #print(' unique id ',uniqueid,' completed  time ',datetime.datetime.now())
    #print(' execution completed for devices at ',datetime.datetime.now())
    #print(' waiting . . .')
    time.sleep(run_frequency)
              
                